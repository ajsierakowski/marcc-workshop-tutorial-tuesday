mport modules
import numpy as np
from matplotlib import pyplot as plt

# set up latex labels
fs = 12
plt.rc('text', usetex=True)
plt.rc('font', family='serif')
xticklabels = plt.getp(plt.gca(), 'xticklabels')
yticklabels = plt.getp(plt.gca(), 'yticklabels')
plt.setp(xticklabels, fontsize=fs)
plt.setp(yticklabels, fontsize=fs)

# use numpy to create data set
N = 100
x = np.zeros(N)
y = np.zeros(N)

for i in range(N):
  x[i] = i * 2.*np.pi / (N-1) 
  y[i] = np.sin(x[i])

# plot with matplotlib
plt.plot(x,y)

# add labels
plt.xlabel(r'$x$')
plt.ylabel(r'$\sin(x)$')
plt.title(r'$y = \sin(x)$')

# save to file
fig = plt.gcf()
fig.set_size_inches(5,4)
fig.savefig('plot.png', bbox_inches='tight')

# show the plot on the screen
plt.show()
