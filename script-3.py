#!/usr/bin/env python3

L = ["JHU", 1876, None]
print(L)
print(L[2] == True)
L[2] = True
print(L)
L.append("Baltimore")
print(L)
print(L.pop(2))
print(L)
L[1] = "Hopkins"
L.sort()
print(L)
