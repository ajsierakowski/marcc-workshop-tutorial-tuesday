#!/bin/bash

a=Hello
b=World

echo $a $b

dir=/home/asiera

ls $dir

# single quotes are literal
c='Hello world'
echo $c

# double quotes substitute variables
d="Big $c"
echo $d

# save contents of command in variable
e=$(ls $dir | wc -l)
echo $e

ls $dir | wc -l
