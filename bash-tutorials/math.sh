#!/bin/bash

# DON'T DO IT! (integer math only)

let a=32/2

echo $a

b=$((5+10))

echo $b

if [ $a -gt $b ] # -gt -lt -eq
then
  echo equal
else
  echo not equal
fi

c="hello"
d="hello"

if [ $c == $d ]
then
  echo the same
else
  echo different
fi

function print_hello {
  echo HELLO!
}

print_hello
print_hello
