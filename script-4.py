#!/usr/bin/env python3

D = {"school": "Hopkins", "nstudents": 12345}
print(D)
print(D["school"])
print(D["nstudents"])
D["buildings"] = ["Bloomberg", "Malone", "Latrobe"]
print(D)
