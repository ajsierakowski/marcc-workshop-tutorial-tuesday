#!/usr/bin/env python3

##################
# DEFINE FUNCTIONS
##################

# get user input
def get_input():
  return input("Type something: ")

# is this a string or character?
def check_length(string):
  if len(string) < 1:
    print("  Empty")
  elif len(string) < 2:
    print("  Character")
  else:
    print("  String")

##############
# BEGIN SCRIPT
##############

# get user input
user_in = get_input()

# repeat until a "q" appears
while user_in.find("q") == -1:

  # check length
  check_length(user_in)

  # print each character from input
  print(list(range(len(user_in))))

  for i in range(len(user_in)):
    print("    user_in[" + str(i) + "] = " + user_in[i])

  # print a different way
  for char in user_in:
    print("    " + char)

  user_in = get_input()

print("Quitting")
