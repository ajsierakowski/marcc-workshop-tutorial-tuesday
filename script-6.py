#!/usr/bin/env python3

# get user input
user_in = input("Type something: ")

# repeat until a "q" appears
while user_in.find("q") == -1:

  # is this a string or character?
  if len(user_in) < 1:
    print("  Empty")
  elif len(user_in) < 2:
    print("  Character")
  else:
    print("  String")

  # print each character from input
  print(list(range(len(user_in))))

  for i in range(len(user_in)):
    print("    user_in[" + str(i) + "] = " + user_in[i])

  # print a different way
  for char in user_in:
    print("    " + char)

  user_in = input("Type something: ")

print("Quitting")
