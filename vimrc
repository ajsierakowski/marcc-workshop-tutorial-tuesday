" .vimrc file
" Save this file as ~/.vimrc
set number
set printoptions=number:y,left:5pc,paper:letter
set shiftwidth=2
set tabstop=2
set expandtab
set scrolloff=15
set background=dark
set title

" Overlength highlighting
" -- Highlights more than 80 characters in a terminal
let g:overlength_enabled = 1
highlight OverLength ctermbg=red ctermfg=white guibg=#592929
match OverLength /\%81v.\+/

function! ToggleOverLength()
    if g:overlength_enabled == 0
        match OverLength /\%81v.\+/
        let g:overlength_enabled = 1
        echo 'OverLength highlighting turned on'
    else
        match
        let g:overlength_enabled = 0
        echo 'OverLength highlighting turned off'
    endif
endfunction

nnoremap <leader>h :call ToggleOverLength()<CR>
