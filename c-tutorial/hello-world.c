#include <stdlib.h>
#include <stdio.h>

/* compile with:
 * gcc -std=c99 -o hello-world hello-world.c
 */

// this is a one-line comment
/* this is a multi-line
 * comment
 */
int main(int argc, char *argv[]) 
{
  printf("Hello world!\n");

  // parse the command line arguments
  printf("There are %d command line arguments.\n", argc);
  printf("They are:\n");

  // i++ <==> i = i + 1
  for(int i = 0; i < argc; i++) {
    printf("  %d: %s\n", i, argv[i]);
  }

  // static array
  int A[10];

  // fill and print A
  for(int i = 0; i < 10; i++) {
    A[i] = i;
    printf("A[%d] = %d\n", i, A[i]);
  }

  // variable array
  int B[argc];

  // fill and print B
  for(int i = 0; i < argc; i++) {
    B[i] = i;
    printf("B[%d] = %d\n", i, B[i]);
  }

  // dynamic array
  int *C; // declared that we have a memory location that will contain integers
  if(argc > 1) {
    int clen = atoi(argv[1]);
    
    // allocate C
    C = malloc(clen * sizeof(int));

    // fill and print C
    for(int i = 0; i < clen; i++) {
      C[i] = i;
      printf("C[%d] = %d\n", i, C[i]);
    }

    // release C
    free(C);
  }

  return EXIT_SUCCESS;
}
