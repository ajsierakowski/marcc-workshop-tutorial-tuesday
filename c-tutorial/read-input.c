#include <stdlib.h>
#include <stdio.h>

#include <factorial.h>

#include "readwrite.h"

int main(int argc, char *argv[])
{
  float a = 0;
  float b = 0;
  float c = 0;

  printf("a = %f\nb = %f\nc = %f\n", a, b, c);

  read("input", &a, &b, &c);

  printf("a = %f\nb = %f\nc = %f\n", a, b, c);

  printf("3! = %d\n", factorial(3));

  return EXIT_SUCCESS;
}
