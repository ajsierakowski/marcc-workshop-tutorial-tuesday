#!/bin/bash

#SBATCH --job-name=tutorial
#SBATCH --time=0:10:0
#SBATCH --partition=gpu
#SBATCH --gres:gpu=1
#SBATCH --nodes=1
# number of tasks (processes) per node
#SBATCH --ntasks-per-node=1
# number of cpus (threads) per task (process)
#SBATCH --cpus-per-task=6
#SBATCH --mail-type=all
#SBATCH --mail-user=sierakowski@jhu.edu
#SBATCH --output=tutorial.out

#### load and unload modules you may need
module load matlab
module list

#### execute code and write output file
hello.sh
echo "Finished with job $SLURM_JOBID"

#### mpiexec by default launches number of tasks requested
